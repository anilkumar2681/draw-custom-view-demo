package com.ultimatecrazy.draw_demo

import android.content.Context
import android.graphics.*
import android.os.Build
import android.view.View
import androidx.annotation.RequiresApi


class MyView(context: Context) : View(context) {

    private val fillPaint: Paint = Paint()
    private val borderPaint = Paint()
    private val vectorPath = Path()
    private val drawMatrix = Matrix()


    init {
        fillPaint.style = Paint.Style.FILL
        fillPaint.color = Color.RED
        fillPaint.isAntiAlias = true
        fillPaint.isDither = true
        borderPaint.style = Paint.Style.STROKE
        borderPaint.strokeWidth = 10f
        borderPaint.color = Color.YELLOW
        borderPaint.isAntiAlias = true
        borderPaint.isDither = true
        vectorPath.moveTo(6.5f, 79.99f)
        vectorPath.lineTo(37.21f, 50.5f)
        vectorPath.lineTo(6.5f, 19.79f)
        vectorPath.lineTo(18.79f, 7.5f)
        vectorPath.lineTo(49.5f, 38.21f)
        vectorPath.lineTo(80.21f, 7.5f)
        vectorPath.lineTo(92.5f, 19.79f)
        vectorPath.lineTo(61.79f, 50.5f)
        vectorPath.lineTo(92.5f, 79.99f)
        vectorPath.lineTo(80.21f, 93.5f)
        vectorPath.lineTo(49.5f, 62.79f)
        vectorPath.lineTo(18.79f, 93.5f)
        vectorPath.close()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        // Calculate a transformation scale between [0, 0, 100, 100] and [0, 0, width, height].
        val scaleX = width / 100.0f
        val scaleY = height / 100.0f
        // Create the transformation matrix.
        drawMatrix.setScale(scaleX, scaleY)
        // Now transform the vector path.
        vectorPath.transform(drawMatrix)
        canvas.drawPath(vectorPath, fillPaint)
        // Then overlap this with the border path.
        canvas.drawPath(vectorPath, borderPaint)


    }


}